import { combineReducers } from 'redux'
import mainReducer from './mainReducer'
import issueReducer from './issueReducer'

const reducer =  combineReducers({
  mainReducer,
  issueReducer
});

export default reducer;