import React from 'react';
import { connect } from 'react-redux';
import { issueDetailRequest } from '../../actions'
import Skeleton from 'react-loading-skeleton';
import moment from 'moment';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import s from '../styles.scss';

class Issue extends React.Component {
    
    componentDidMount() {
        const { repos, name, num } = this.props.router.query;
        this.props.dispatch(issueDetailRequest(repos, name, num))
    }

    render() {
        const {
            state,
            body,
            author_association,
            created_at,
            title,
            number,
            login,
            avatar_url,
            loading } = this.props.issueReducer;
        return(
            <div className={s.wrapper}>
                <div className={s.issueWrapper}>{!loading ?
                    (
                        <h2 className={s.titleIssue}>{title} 
                            <span>   # {number}</span>
                        </h2>
                    )
                    : (
                        <Skeleton height={29} />
                        )
                }
                    <div className={s.statusIssue}>
                        {!loading ?
                        (
                            <div className={s.outlineIcon}>
                                <ErrorOutlineIcon />
                                {state}
                            </div>
                        ) : (<Skeleton height={29} />)
                        }
                        {!loading ?
                        (
                            <div className={s.login}>{login}</div>
                        ) : (<Skeleton height={29} />)
                        }
                        {!loading ?
                        (
                            <div className={s.moment}>
                                opened this issue 
                                {moment(created_at).fromNow()}
                            </div>
                        ) : (<Skeleton height={29} />)
                        }
                        
                    </div>
                    <div className={s.imageWrapper}>
                    {!loading ?
                        (
                            <div className={s.image} style={{
                                background: `url('${avatar_url}')`,
                                }}
                            />
                        ) : (<Skeleton height={29} />)
                    }
                        <ArrowBackIosIcon className={s.arrowBack} />
                        {!loading ?
                        (
                            <div className={s.person}>
                                <div className={s.commentBlock}>
                                    {login}
                                </div>
                                <span className={s.commentMoment}>
                                    comented this issue 
                                    {moment(created_at).fromNow()}
                                </span>
                            </div>
                        ) : (<Skeleton height={29} />)
                        }
                        {!loading ?
                        (
                            <div className={s.body}>
                                {body}
                            </div>
                        ) : (<Skeleton height={350} />)
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(state => state)(Issue)