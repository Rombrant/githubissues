import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    enterName,
    enterRepos,
    fetchCommonIssuesRequest,
    chooseItemPerPage,
    loadDataRequest,
    choosePage } from '../actions'
import cx from 'classnames';
import Skeleton from 'react-loading-skeleton';
import Searching from '../components/searching';
import FindReplaceIcon from '@material-ui/icons/FindReplace';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import List from '../components/list';
import s from './styles.scss';

class Index extends Component {

    inputChange = (event, key) => {
        event.persist();
        const value = event.target.value.trim().replace(/ +/g, ' ')
        switch(key) {
            case'repos':
                this.props.dispatch(enterRepos(value))
                break
            case'name':
            this.props.dispatch(enterName(value))
                break
        }
    };

    fetchResult = async (event, code) => {
        event.persist();
        const { repos, name, perPage } = this.props;
        if (repos.length && name.length && (event.charCode === 13 || code)) {
            this.props.dispatch(fetchCommonIssuesRequest())
        }
    };

    reFetchedIssues = (count) => {
        this.props.dispatch(chooseItemPerPage(count));
        this.props.dispatch(loadDataRequest())
    }
    
    choosePage = (page) => {
        this.props.dispatch(choosePage(page))
        this.props.dispatch(loadDataRequest())
    }

    chooseItemsPerPage =(count) => {
        this.props.dispatch(chooseItemPerPage(count))
    }

    render() {
        const {
            perPage,
            pagination,
            curPage,
            isFetched,
            loading,
            error,
            list,
            repos,
            name } = this.props;
        return (
            <div>
                <div className={s.block}>
                    <Searching 
                        key="repos" 
                        fetchResult={this.fetchResult} 
                        inputChange={(event) => this.inputChange(event, 'repos')} 
                    />
                    <div style={{ background: '#ffffff', padding: '10px 0' }}>/</div>
                    <Searching 
                        key="name"
                        fetchResult={this.fetchResult}
                        inputChange={(event) => this.inputChange(event, 'name')}
                    />
                    <Button 
                        className={s.searchButton} 
                        onClick={(event) => this.fetchResult(event, 'fetch')} 
                        style={{ margin: '0 10px' }} 
                        variant="contained"
                    >
                        <FindReplaceIcon />
                    </Button>
                </div>
                <div className={s.content}>
                    <div className={s.controls}>
                        {['10', '30', '50'].map((e, index) => (
                            <Button 
                                key={index}
                                className={cx( {[s.active]: e === perPage} )}
                                onClick={() => !isFetched ? this.chooseItemsPerPage(e) : this.reFetchedIssues(e)}
                                style={{ margin: '0 10px' }}
                                variant="contained">{e}</Button>
                        ))}
                    </div>
                    {
                        !loading && isFetched ?
                    (
                        <div>
                            <List list={list} repos={repos} name={name} />
                            <div className={s.controls} style={{ flexWrap: 'wrap' }}>
                                {pagination.map((e, index) => (
                                    <Button
                                        key={index}
                                        className={cx( {[s.active]: e.toString() === curPage}, s.pagination )}
                                        onClick={() => this.choosePage(e.toString())}
                                        variant="contained">
                                        {e}
                                    </Button>
                                ))}
                            </div>
                        </div>
                    )
                        : loading ? (
                            <Skeleton height={60} count={Number(perPage)} />
                        ) : ''
                    }
                </div>
                {
                    error ?
                (
                    <div className={s.error}>
                        <Alert severity="error">{error}</Alert>
                    </div>
                ) : ''
                }
            </div>
        )
    }
};



export default connect(state => state.mainReducer)(Index)