  
/* global fetch */

import { all, call, delay, put, take, takeLatest, select } from 'redux-saga/effects'
import es6promise from 'es6-promise'
import 'isomorphic-unfetch'

import {
  actionTypes,
  fetchCommonIssuesFail,
  fetchCommonIssuesSuccess,
  loadDataSuccess,
  loadDataFail,
  loadDataRequest,
  issueDetailFail,
  issueDetailSuccess } from './actions'

es6promise.polyfill()

export const getProject = (state) => state.mainReducer
export const getIssue = (state) => state.issueReducer


function* fetchCommonIssuesInfoSagas() {
  while(true) {
    yield take(actionTypes.COMMON_ISSUES_INFO_REQUEST)
    try {
      let totalpage;
      let pagination = [];
      let total;
      const { repos, name, perPage } = yield select(getProject)
      const path = `https://api.github.com/repos/${repos}/${name}`;
      const res = yield fetch(path);
      if (res.status === 200) {
        const data = yield res.json();
        total = data.open_issues_count;
        totalpage = Math.round(data.open_issues_count / perPage);
        for ( let i = 0; totalpage > i; i++) {
            pagination.push(i+1);
        }
        yield put(fetchCommonIssuesSuccess(pagination, total))
        yield put(loadDataRequest())
      } else {
        yield put(fetchCommonIssuesFail())
      }
    } catch(err) {
      yield put(fetchCommonIssuesFail())
    }
  }
}

function* loadDataSaga() {
  while(true) {
    yield take(actionTypes.LOAD_DATA_REQUEST)
    try {
      const { repos, name, perPage, curPage } = yield select(getProject);
      const path = `https://api.github.com/repos/${repos}/${name}/issues?page=${curPage}&per_page=${perPage}`;
      const res = yield fetch(path)
      if(res.status === 200) {
        const data = yield res.json()
        const list = [];
        data ? 
        data.forEach(element => {
            list.push({ avatar: element.user.avatar_url, title: element.title, date: element.created_at, number: element.number })
        }) : () => {};
        yield put(loadDataSuccess(list))
      } else {
        yield put(loadDataFail())
      }
    } catch (err) {
      yield put(loadDataFail())
    }
  }
}

function* loadIssueDetailSaga() {
  while(true) {
    yield take(actionTypes.ISSUE_DETAIL_REQUEST)
    try {
      const { name, repos, num } = yield select(getIssue);
      const path = `https://api.github.com/repos/${repos}/${name}/issues/${num}`;
      const res = yield fetch(path)
      if(res.status === 200) {
        const data = yield res.json()
        const issue = {
          state: data.state,
          body: data.body,
          author_association: data.author_association,
          created_at: data.created_at,
          title: data.title,
          number: data.number,
          login: data.user.login,
          avatar_url: data.user.avatar_url
        }
        yield put(issueDetailSuccess(issue))
      } else {
        yield put(issueDetailFail())
      }
    } catch (err) {
      yield put(issueDetailFail())
    }
  }
}

function* rootSaga() {
  yield all([
    fetchCommonIssuesInfoSagas(),
    loadDataSaga(),
    loadIssueDetailSaga()
  ])
}

export default rootSaga