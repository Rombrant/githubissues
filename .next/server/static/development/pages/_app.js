module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./actions.js":
/*!********************!*\
  !*** ./actions.js ***!
  \********************/
/*! exports provided: actionTypes, enterName, enterRepos, chooseItemPerPage, fetchCommonIssuesRequest, fetchCommonIssuesFail, fetchCommonIssuesSuccess, loadDataRequest, loadDataSuccess, loadDataFail, choosePage, choosePath, issueDetailRequest, issueDetailSuccess, issueDetailFail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actionTypes", function() { return actionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "enterName", function() { return enterName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "enterRepos", function() { return enterRepos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chooseItemPerPage", function() { return chooseItemPerPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCommonIssuesRequest", function() { return fetchCommonIssuesRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCommonIssuesFail", function() { return fetchCommonIssuesFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCommonIssuesSuccess", function() { return fetchCommonIssuesSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDataRequest", function() { return loadDataRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDataSuccess", function() { return loadDataSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDataFail", function() { return loadDataFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "choosePage", function() { return choosePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "choosePath", function() { return choosePath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "issueDetailRequest", function() { return issueDetailRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "issueDetailSuccess", function() { return issueDetailSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "issueDetailFail", function() { return issueDetailFail; });
const actionTypes = {
  ENTER_NAME: 'ENTER_NAME',
  ENTER_REPOS: 'ENTER_REPOS',
  CHOOSE_ITEM_PERPAGE: 'CHOOSE_ITEM_PERPAGE',
  COMMON_ISSUES_INFO_REQUEST: 'COMMON_ISSUES_INFO_REQUEST',
  COMMON_ISSUES_INFO_SUCCESS: 'COMMON_ISSUES_INFO_SUCCESS',
  COMMON_ISSUES_INFO_FAIL: 'COMMON_ISSUES_INFO_FAIL',
  LOAD_DATA_REQUEST: 'LOAD_DATA_REQUEST',
  LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
  LOAD_DATA_FAIL: 'LOAD_DATA_FAIL',
  CHOOSE_PAGE: 'CHOOSE_PAGE',
  CHOOSE_PATH: 'CHOOSE_PATH',
  ISSUE_DETAIL_REQUEST: 'ISSUE_DETAIL_REQUEST',
  ISSUE_DETAIL_SUCCESS: 'ISSUE_DETAIL_SUCCESS',
  ISSUE_DETAIL_FAIL: 'ISSUE_DETAIL_FAIL'
};
function enterName(name) {
  return {
    type: actionTypes.ENTER_NAME,
    name
  };
}
function enterRepos(repos) {
  return {
    type: actionTypes.ENTER_REPOS,
    repos
  };
}
function chooseItemPerPage(count) {
  return {
    type: actionTypes.CHOOSE_ITEM_PERPAGE,
    count
  };
}
function fetchCommonIssuesRequest() {
  return {
    type: actionTypes.COMMON_ISSUES_INFO_REQUEST
  };
}
function fetchCommonIssuesFail() {
  return {
    type: actionTypes.COMMON_ISSUES_INFO_FAIL
  };
}
function fetchCommonIssuesSuccess(pagination, total) {
  return {
    type: actionTypes.COMMON_ISSUES_INFO_SUCCESS,
    payload: {
      pagination: pagination,
      total: total
    }
  };
}
function loadDataRequest() {
  return {
    type: actionTypes.LOAD_DATA_REQUEST
  };
}
function loadDataSuccess(list) {
  return {
    type: actionTypes.LOAD_DATA_SUCCESS,
    list
  };
}
function loadDataFail() {
  return {
    type: actionTypes.LOAD_DATA_FAIL
  };
}
function choosePage(page) {
  return {
    type: actionTypes.CHOOSE_PAGE,
    page
  };
}
function choosePath(num) {
  return {
    type: actionTypes.CHOOSE_PATH,
    num
  };
}
function issueDetailRequest(repos, name, num) {
  return {
    type: actionTypes.ISSUE_DETAIL_REQUEST,
    payload: {
      repos: repos,
      name: name,
      num: num
    }
  };
}
function issueDetailSuccess(issue) {
  return {
    type: actionTypes.ISSUE_DETAIL_SUCCESS,
    issue
  };
}
function issueDetailFail() {
  return {
    type: actionTypes.ISSUE_DETAIL_FAIL
  };
}

/***/ }),

/***/ "./components/layout/index.js":
/*!************************************!*\
  !*** ./components/layout/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Layout; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _pages_styles_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../pages/styles.scss */ "./pages/styles.scss");
/* harmony import */ var _pages_styles_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_pages_styles_scss__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "C:\\Users\\feel_\\OneDrive\\Desktop\\projects\\githubissues\\components\\layout\\index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


function Layout(props) {
  return __jsx("div", {
    className: _pages_styles_scss__WEBPACK_IMPORTED_MODULE_1___default.a.wrapper,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 9
    }
  }, __jsx("header", {
    className: _pages_styles_scss__WEBPACK_IMPORTED_MODULE_1___default.a.header,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: _pages_styles_scss__WEBPACK_IMPORTED_MODULE_1___default.a.gitLogo,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 17
    }
  }), __jsx("h1", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 17
    }
  }, "Github Issue Tracker")), props.children);
}
;

/***/ }),

/***/ "./issueReducer.js":
/*!*************************!*\
  !*** ./issueReducer.js ***!
  \*************************/
/*! exports provided: exampleInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "exampleInitialState", function() { return exampleInitialState; });
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./actions */ "./actions.js");
function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}


const exampleInitialState = {
  state: '',
  body: '',
  author_association: '',
  created_at: '',
  title: '',
  number: '',
  login: '',
  avatar_url: '',
  loading: false,
  error: false,
  name: '',
  repos: '',
  num: ''
};

function issueReducer(state = exampleInitialState, action) {
  switch (action.type) {
    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].ISSUE_DETAIL_REQUEST:
      return _objectSpread({}, state, {
        name: action.payload.name,
        repos: action.payload.repos,
        num: action.payload.num,
        loading: true,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].ISSUE_DETAIL_SUCCESS:
      return _objectSpread({}, action.issue, {
        loading: false,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].ISSUE_DETAIL_FAIL:
      return _objectSpread({}, state, {
        loading: false,
        error: true
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (issueReducer);

/***/ }),

/***/ "./mainReducer.js":
/*!************************!*\
  !*** ./mainReducer.js ***!
  \************************/
/*! exports provided: exampleInitialState, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "exampleInitialState", function() { return exampleInitialState; });
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./actions */ "./actions.js");
function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}


const exampleInitialState = {
  repos: '',
  name: '',
  list: [],
  perPage: '10',
  total: '',
  pagination: [],
  curPage: '1',
  isFetched: false,
  loading: false,
  path: '',
  error: false
};

function mainReducer(state = exampleInitialState, action) {
  switch (action.type) {
    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].ENTER_NAME:
      return _objectSpread({}, state, {
        name: action.name,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].ENTER_REPOS:
      return _objectSpread({}, state, {
        repos: action.repos,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].CHOOSE_PAGE:
      return _objectSpread({}, state, {
        curPage: action.page,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].CHOOSE_PATH:
      const {
        repos,
        name
      } = state;
      const {
        num
      } = action;
      const path = `https://api.github.com/repos/${repos}/${name}/issues/${num}`;
      return _objectSpread({}, state, {
        path: path,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].CHOOSE_ITEM_PERPAGE:
      const {
        total
      } = state;
      let pagination = [];

      if (total) {
        const {
          count
        } = action;
        const totalpage = Math.round(Number(total) / Number(count));

        for (let i = 0; totalpage > i; i++) {
          pagination.push(i + 1);
        }
      }

      return _objectSpread({}, state, {
        perPage: action.count,
        pagination: pagination,
        curPage: '1'
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].COMMON_ISSUES_INFO_REQUEST:
      return _objectSpread({}, state, {
        loading: true,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].COMMON_ISSUES_INFO_SUCCESS:
      return _objectSpread({}, state, {
        pagination: action.payload.pagination,
        total: action.payload.total,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].COMMON_ISSUES_INFO_FAIL:
      return _objectSpread({}, state, {
        loading: false,
        error: 'Something went wrong please reboot your page'
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].LOAD_DATA_REQUEST:
      return _objectSpread({}, state, {
        loading: true,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].LOAD_DATA_SUCCESS:
      return _objectSpread({}, state, {
        list: action.list,
        isFetched: true,
        loading: false,
        error: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_0__["actionTypes"].LOAD_DATA_FAIL:
      return _objectSpread({}, state, {
        loading: false,
        error: 'Something went wrong please reboot your page'
      });

    default:
      return state;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (mainReducer);

/***/ }),

/***/ "./node_modules/next/app.js":
/*!**********************************!*\
  !*** ./node_modules/next/app.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/pages/_app */ "./node_modules/next/dist/pages/_app.js");

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/utils.js":
/*!*********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/utils.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result = null;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn.apply(this, args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (true) {
    if ((_a = App.prototype) === null || _a === void 0 ? void 0 : _a.getInitialProps) {
      const message = `"${getDisplayName(App)}.getInitialProps()" is defined as an instance method - visit https://err.sh/zeit/next.js/get-initial-props-as-an-instance-method for more information.`;
      throw new Error(message);
    }
  } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (true) {
    if (Object.keys(props).length === 0 && !ctx.ctx) {
      console.warn(`${getDisplayName(App)} returned an empty object from \`getInitialProps\`. This de-optimizes and prevents automatic static optimization. https://err.sh/zeit/next.js/empty-object-getInitialProps`);
    }
  }

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (true) {
    if (url !== null && typeof url === 'object') {
      Object.keys(url).forEach(key => {
        if (exports.urlObjectKeys.indexOf(key) === -1) {
          console.warn(`Unknown key passed via urlObject into url.format: ${key}`);
        }
      });
    }
  }

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "./node_modules/next/dist/pages/_app.js":
/*!**********************************************!*\
  !*** ./node_modules/next/dist/pages/_app.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/next/node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.Container = Container;
exports.createUrl = createUrl;
exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "./node_modules/next/dist/next-server/lib/utils.js");

exports.AppInitialProps = _utils.AppInitialProps;
/**
* `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.
* This allows for keeping state between navigation, custom error handling, injecting additional data.
*/

async function appGetInitialProps(_ref) {
  var {
    Component,
    ctx
  } = _ref;
  var pageProps = await (0, _utils.loadGetInitialProps)(Component, ctx);
  return {
    pageProps
  };
}

class App extends _react.default.Component {
  // Kept here for backwards compatibility.
  // When someone ended App they could call `super.componentDidCatch`.
  // @deprecated This method is no longer needed. Errors are caught at the top level
  componentDidCatch(error, _errorInfo) {
    throw error;
  }

  render() {
    var {
      router,
      Component,
      pageProps,
      __N_SSG,
      __N_SSP
    } = this.props;
    return _react.default.createElement(Component, Object.assign({}, pageProps, // we don't add the legacy URL prop if it's using non-legacy
    // methods like getStaticProps and getServerSideProps
    !(__N_SSG || __N_SSP) ? {
      url: createUrl(router)
    } : {}));
  }

}

exports.default = App;
App.origGetInitialProps = appGetInitialProps;
App.getInitialProps = appGetInitialProps;
var warnContainer;
var warnUrl;

if (true) {
  warnContainer = (0, _utils.execOnce)(() => {
    console.warn("Warning: the `Container` in `_app` has been deprecated and should be removed. https://err.sh/zeit/next.js/app-container-deprecated");
  });
  warnUrl = (0, _utils.execOnce)(() => {
    console.error("Warning: the 'url' property is deprecated. https://err.sh/zeit/next.js/url-deprecated");
  });
} // @deprecated noop for now until removal


function Container(p) {
  if (true) warnContainer();
  return p.children;
}

function createUrl(router) {
  // This is to make sure we don't references the router object at call time
  var {
    pathname,
    asPath,
    query
  } = router;
  return {
    get query() {
      if (true) warnUrl();
      return query;
    },

    get pathname() {
      if (true) warnUrl();
      return pathname;
    },

    get asPath() {
      if (true) warnUrl();
      return asPath;
    },

    back: () => {
      if (true) warnUrl();
      router.back();
    },
    push: (url, as) => {
      if (true) warnUrl();
      return router.push(url, as);
    },
    pushTo: (href, as) => {
      if (true) warnUrl();
      var pushRoute = as ? href : '';
      var pushUrl = as || href;
      return router.push(pushRoute, pushUrl);
    },
    replace: (url, as) => {
      if (true) warnUrl();
      return router.replace(url, as);
    },
    replaceTo: (href, as) => {
      if (true) warnUrl();
      var replaceRoute = as ? href : '';
      var replaceUrl = as || href;
      return router.replace(replaceRoute, replaceUrl);
    }
  };
}

/***/ }),

/***/ "./node_modules/next/node_modules/@babel/runtime/helpers/interopRequireDefault.js":
/*!****************************************************************************************!*\
  !*** ./node_modules/next/node_modules/@babel/runtime/helpers/interopRequireDefault.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next/app */ "./node_modules/next/app.js");
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_app__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-redux-wrapper */ "next-redux-wrapper");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_redux_saga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next-redux-saga */ "next-redux-saga");
/* harmony import */ var next_redux_saga__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_redux_saga__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/layout */ "./components/layout/index.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../store */ "./store.js");
var _jsxFileName = "C:\\Users\\feel_\\OneDrive\\Desktop\\projects\\githubissues\\pages\\_app.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}









class MyApp extends next_app__WEBPACK_IMPORTED_MODULE_0___default.a {
  static async getInitialProps({
    Component,
    ctx,
    router
  }) {
    let pageProps = {
      router
    };

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({
        ctx
      });
    }

    return {
      pageProps
    };
  }

  render() {
    const {
      Component,
      pageProps,
      store
    } = this.props;
    return __jsx(react_redux__WEBPACK_IMPORTED_MODULE_2__["Provider"], {
      store: store,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25,
        columnNumber: 7
      }
    }, __jsx(_components_layout__WEBPACK_IMPORTED_MODULE_5__["default"], {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }
    }, __jsx(Component, _extends({}, pageProps, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 11
      }
    }))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3___default()(_store__WEBPACK_IMPORTED_MODULE_6__["default"])(next_redux_saga__WEBPACK_IMPORTED_MODULE_4___default()(MyApp)));

/***/ }),

/***/ "./pages/styles.scss":
/*!***************************!*\
  !*** ./pages/styles.scss ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"titleIssue": "_13IBBD-c10lpDX5Cx8Ddqb",
	"statusIssue": "_1_P0jWnr9P8KbAlGpLoSD8",
	"outlineIcon": "_3dw4_2_kH-scykKXcXluFZ",
	"login": "-jnXChe1iZx07l7U3NRjo",
	"moment": "_1OLda4PJWC8GvqKjV-lf47",
	"imageWrapper": "_1RbjjqyDH02az8hoVjoCsX",
	"arrowBack": "_2GZz4xBd_IXrhGxYIdBmZv",
	"commentBlock": "_1pBYyLbGevSs2tlaC7uAXq",
	"commentMoment": "_3GYY6S2Z1-PmqoIgWM_hxF",
	"aboutCompany": "JfIMAW3O728D4W8InCBwa",
	"aboutImg": "_2lg5DbPeWUMXxzgOpA9HFF",
	"textAbout": "_13DEwQ8FxGQtzgcrYQ5RD",
	"breadcrumbs": "JIzGuMrWaJnwJvtyxj75i",
	"breadcrumbsLinks": "gKYDiVPy1OlQoHgxgjHim",
	"active": "_26Tc2kOxRMQ-eylVkbvMYl",
	"block": "BWB4CFqryEzsafnCu19EH",
	"header": "_2jJ0u2IP9EFPvnZBY8TOVf",
	"gitLogo": "_2fLcD8ryI1jWx26Utl1HgB",
	"wrapper": "_2a_plItHO16c59wIwMCSvx",
	"content": "_3xqyrH14vpQPdPdkv21GGO",
	"controls": "NhWg3wrkjBntoI1ymaaHM",
	"issueWrapper": "_3OTRj7cXo7rqGkViAe7-QD",
	"image": "_1UUknJgo4eYv1kXqtQjddS",
	"person": "_3Qq-uBj6FvSn7JNUn0Cb5O",
	"error": "_2q8zl7qJI59LE-rO5CWLq5",
	"body": "_1s_UOdFxuK0GKll0UEZuUt",
	"pagination": "_2gtQh16urWoWz3k5HHktNM",
	"searchButton": "_2Z3QeIiN1fRdNjIZJxXPQl"
};

/***/ }),

/***/ "./reducer.js":
/*!********************!*\
  !*** ./reducer.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mainReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mainReducer */ "./mainReducer.js");
/* harmony import */ var _issueReducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./issueReducer */ "./issueReducer.js");



const reducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  mainReducer: _mainReducer__WEBPACK_IMPORTED_MODULE_1__["default"],
  issueReducer: _issueReducer__WEBPACK_IMPORTED_MODULE_2__["default"]
});
/* harmony default export */ __webpack_exports__["default"] = (reducer);

/***/ }),

/***/ "./saga.js":
/*!*****************!*\
  !*** ./saga.js ***!
  \*****************/
/*! exports provided: getProject, getIssue, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProject", function() { return getProject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getIssue", function() { return getIssue; });
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var es6_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! es6-promise */ "es6-promise");
/* harmony import */ var es6_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(es6_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./actions */ "./actions.js");
/* global fetch */




es6_promise__WEBPACK_IMPORTED_MODULE_1___default.a.polyfill();
const getProject = state => state.mainReducer;
const getIssue = state => state.issueReducer;

function* fetchCommonIssuesInfoSagas() {
  while (true) {
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["take"])(_actions__WEBPACK_IMPORTED_MODULE_3__["actionTypes"].COMMON_ISSUES_INFO_REQUEST);

    try {
      let totalpage;
      let pagination = [];
      let total;
      const {
        repos,
        name,
        perPage
      } = yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["select"])(getProject);
      const path = `https://api.github.com/repos/${repos}/${name}`;
      const res = yield fetch(path);

      if (res.status === 200) {
        const data = yield res.json();
        total = data.open_issues_count;
        totalpage = Math.round(data.open_issues_count / perPage);

        for (let i = 0; totalpage > i; i++) {
          pagination.push(i + 1);
        }

        yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["fetchCommonIssuesSuccess"])(pagination, total));
        yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["loadDataRequest"])());
      } else {
        yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["fetchCommonIssuesFail"])());
      }
    } catch (err) {
      yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["fetchCommonIssuesFail"])());
    }
  }
}

function* loadDataSaga() {
  while (true) {
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["take"])(_actions__WEBPACK_IMPORTED_MODULE_3__["actionTypes"].LOAD_DATA_REQUEST);

    try {
      const {
        repos,
        name,
        perPage,
        curPage
      } = yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["select"])(getProject);
      const path = `https://api.github.com/repos/${repos}/${name}/issues?page=${curPage}&per_page=${perPage}`;
      const res = yield fetch(path);

      if (res.status === 200) {
        const data = yield res.json();
        const list = [];
        data ? data.forEach(element => {
          list.push({
            avatar: element.user.avatar_url,
            title: element.title,
            date: element.created_at,
            number: element.number
          });
        }) : () => {};
        yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["loadDataSuccess"])(list));
      } else {
        yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["loadDataFail"])());
      }
    } catch (err) {
      yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["loadDataFail"])());
    }
  }
}

function* loadIssueDetailSaga() {
  while (true) {
    yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["take"])(_actions__WEBPACK_IMPORTED_MODULE_3__["actionTypes"].ISSUE_DETAIL_REQUEST);

    try {
      const {
        name,
        repos,
        num
      } = yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["select"])(getIssue);
      const path = `https://api.github.com/repos/${repos}/${name}/issues/${num}`;
      const res = yield fetch(path);

      if (res.status === 200) {
        const data = yield res.json();
        const issue = {
          state: data.state,
          body: data.body,
          author_association: data.author_association,
          created_at: data.created_at,
          title: data.title,
          number: data.number,
          login: data.user.login,
          avatar_url: data.user.avatar_url
        };
        yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["issueDetailSuccess"])(issue));
      } else {
        yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["issueDetailFail"])());
      }
    } catch (err) {
      yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["put"])(Object(_actions__WEBPACK_IMPORTED_MODULE_3__["issueDetailFail"])());
    }
  }
}

function* rootSaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_0__["all"])([fetchCommonIssuesInfoSagas(), loadDataSaga(), loadIssueDetailSaga()]);
}

/* harmony default export */ __webpack_exports__["default"] = (rootSaga);

/***/ }),

/***/ "./store.js":
/*!******************!*\
  !*** ./store.js ***!
  \******************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga */ "redux-saga");
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-logger */ "redux-logger");
/* harmony import */ var redux_logger__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_logger__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _reducer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reducer */ "./reducer.js");
/* harmony import */ var _saga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./saga */ "./saga.js");






const bindMiddleware = middleware => {
  if (true) {
    const {
      composeWithDevTools
    } = __webpack_require__(/*! redux-devtools-extension */ "redux-devtools-extension");

    return composeWithDevTools(Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(...middleware, redux_logger__WEBPACK_IMPORTED_MODULE_2___default.a));
  }

  return Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(...middleware, redux_logger__WEBPACK_IMPORTED_MODULE_2___default.a);
};

function configureStore(initialState = {}) {
  const sagaMiddleware = redux_saga__WEBPACK_IMPORTED_MODULE_1___default()();
  const store = Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(_reducer__WEBPACK_IMPORTED_MODULE_3__["default"], initialState, bindMiddleware([sagaMiddleware]));
  store.sagaTask = sagaMiddleware.run(_saga__WEBPACK_IMPORTED_MODULE_4__["default"]);
  return store;
}

/* harmony default export */ __webpack_exports__["default"] = (configureStore);

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi private-next-pages/_app.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.js */"./pages/_app.js");


/***/ }),

/***/ "es6-promise":
/*!******************************!*\
  !*** external "es6-promise" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("es6-promise");

/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "next-redux-saga":
/*!**********************************!*\
  !*** external "next-redux-saga" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-redux-saga");

/***/ }),

/***/ "next-redux-wrapper":
/*!*************************************!*\
  !*** external "next-redux-wrapper" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-devtools-extension":
/*!*******************************************!*\
  !*** external "redux-devtools-extension" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension");

/***/ }),

/***/ "redux-logger":
/*!*******************************!*\
  !*** external "redux-logger" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-logger");

/***/ }),

/***/ "redux-saga":
/*!*****************************!*\
  !*** external "redux-saga" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga");

/***/ }),

/***/ "redux-saga/effects":
/*!*************************************!*\
  !*** external "redux-saga/effects" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga/effects");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=_app.js.map