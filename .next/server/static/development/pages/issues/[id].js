module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./actions.js":
/*!********************!*\
  !*** ./actions.js ***!
  \********************/
/*! exports provided: actionTypes, enterName, enterRepos, chooseItemPerPage, fetchCommonIssuesRequest, fetchCommonIssuesFail, fetchCommonIssuesSuccess, loadDataRequest, loadDataSuccess, loadDataFail, choosePage, choosePath, issueDetailRequest, issueDetailSuccess, issueDetailFail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actionTypes", function() { return actionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "enterName", function() { return enterName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "enterRepos", function() { return enterRepos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chooseItemPerPage", function() { return chooseItemPerPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCommonIssuesRequest", function() { return fetchCommonIssuesRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCommonIssuesFail", function() { return fetchCommonIssuesFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCommonIssuesSuccess", function() { return fetchCommonIssuesSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDataRequest", function() { return loadDataRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDataSuccess", function() { return loadDataSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDataFail", function() { return loadDataFail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "choosePage", function() { return choosePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "choosePath", function() { return choosePath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "issueDetailRequest", function() { return issueDetailRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "issueDetailSuccess", function() { return issueDetailSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "issueDetailFail", function() { return issueDetailFail; });
const actionTypes = {
  ENTER_NAME: 'ENTER_NAME',
  ENTER_REPOS: 'ENTER_REPOS',
  CHOOSE_ITEM_PERPAGE: 'CHOOSE_ITEM_PERPAGE',
  COMMON_ISSUES_INFO_REQUEST: 'COMMON_ISSUES_INFO_REQUEST',
  COMMON_ISSUES_INFO_SUCCESS: 'COMMON_ISSUES_INFO_SUCCESS',
  COMMON_ISSUES_INFO_FAIL: 'COMMON_ISSUES_INFO_FAIL',
  LOAD_DATA_REQUEST: 'LOAD_DATA_REQUEST',
  LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
  LOAD_DATA_FAIL: 'LOAD_DATA_FAIL',
  CHOOSE_PAGE: 'CHOOSE_PAGE',
  CHOOSE_PATH: 'CHOOSE_PATH',
  ISSUE_DETAIL_REQUEST: 'ISSUE_DETAIL_REQUEST',
  ISSUE_DETAIL_SUCCESS: 'ISSUE_DETAIL_SUCCESS',
  ISSUE_DETAIL_FAIL: 'ISSUE_DETAIL_FAIL'
};
function enterName(name) {
  return {
    type: actionTypes.ENTER_NAME,
    name
  };
}
function enterRepos(repos) {
  return {
    type: actionTypes.ENTER_REPOS,
    repos
  };
}
function chooseItemPerPage(count) {
  return {
    type: actionTypes.CHOOSE_ITEM_PERPAGE,
    count
  };
}
function fetchCommonIssuesRequest() {
  return {
    type: actionTypes.COMMON_ISSUES_INFO_REQUEST
  };
}
function fetchCommonIssuesFail() {
  return {
    type: actionTypes.COMMON_ISSUES_INFO_FAIL
  };
}
function fetchCommonIssuesSuccess(pagination, total) {
  return {
    type: actionTypes.COMMON_ISSUES_INFO_SUCCESS,
    payload: {
      pagination: pagination,
      total: total
    }
  };
}
function loadDataRequest() {
  return {
    type: actionTypes.LOAD_DATA_REQUEST
  };
}
function loadDataSuccess(list) {
  return {
    type: actionTypes.LOAD_DATA_SUCCESS,
    list
  };
}
function loadDataFail() {
  return {
    type: actionTypes.LOAD_DATA_FAIL
  };
}
function choosePage(page) {
  return {
    type: actionTypes.CHOOSE_PAGE,
    page
  };
}
function choosePath(num) {
  return {
    type: actionTypes.CHOOSE_PATH,
    num
  };
}
function issueDetailRequest(repos, name, num) {
  return {
    type: actionTypes.ISSUE_DETAIL_REQUEST,
    payload: {
      repos: repos,
      name: name,
      num: num
    }
  };
}
function issueDetailSuccess(issue) {
  return {
    type: actionTypes.ISSUE_DETAIL_SUCCESS,
    issue
  };
}
function issueDetailFail() {
  return {
    type: actionTypes.ISSUE_DETAIL_FAIL
  };
}

/***/ }),

/***/ "./pages/issues/[id].js":
/*!******************************!*\
  !*** ./pages/issues/[id].js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../actions */ "./actions.js");
/* harmony import */ var react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-loading-skeleton */ "react-loading-skeleton");
/* harmony import */ var react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_icons_ErrorOutline__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/icons/ErrorOutline */ "@material-ui/icons/ErrorOutline");
/* harmony import */ var _material_ui_icons_ErrorOutline__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ErrorOutline__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_icons_ArrowBackIos__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/ArrowBackIos */ "@material-ui/icons/ArrowBackIos");
/* harmony import */ var _material_ui_icons_ArrowBackIos__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ArrowBackIos__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../styles.scss */ "./pages/styles.scss");
/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_styles_scss__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "C:\\Users\\feel_\\OneDrive\\Desktop\\projects\\githubissues\\pages\\issues\\[id].js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;









class Issue extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  componentDidMount() {
    const {
      repos,
      name,
      num
    } = this.props.router.query;
    this.props.dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_2__["issueDetailRequest"])(repos, name, num));
  }

  render() {
    const {
      state,
      body,
      author_association,
      created_at,
      title,
      number,
      login,
      avatar_url,
      loading
    } = this.props.issueReducer;
    return __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.wrapper,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 29,
        columnNumber: 13
      }
    }, __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.issueWrapper,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 17
      }
    }, !loading ? __jsx("h2", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.titleIssue,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 25
      }
    }, title, __jsx("span", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 29
      }
    }, "   # ", number)) : __jsx(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3___default.a, {
      height: 29,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 25
      }
    }), __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.statusIssue,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 21
      }
    }, !loading ? __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.outlineIcon,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 29
      }
    }, __jsx(_material_ui_icons_ErrorOutline__WEBPACK_IMPORTED_MODULE_5___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 33
      }
    }), state) : __jsx(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3___default.a, {
      height: 29,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 30
      }
    }), !loading ? __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.login,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51,
        columnNumber: 29
      }
    }, login) : __jsx(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3___default.a, {
      height: 29,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 52,
        columnNumber: 30
      }
    }), !loading ? __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.moment,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 29
      }
    }, "opened this issue", moment__WEBPACK_IMPORTED_MODULE_4___default()(created_at).fromNow()) : __jsx(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3___default.a, {
      height: 29,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 30
      }
    })), __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.imageWrapper,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64,
        columnNumber: 21
      }
    }, !loading ? __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.image,
      style: {
        background: `url('${avatar_url}')`
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67,
        columnNumber: 29
      }
    }) : __jsx(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3___default.a, {
      height: 29,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71,
        columnNumber: 30
      }
    }), __jsx(_material_ui_icons_ArrowBackIos__WEBPACK_IMPORTED_MODULE_6___default.a, {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.arrowBack,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73,
        columnNumber: 25
      }
    }), !loading ? __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.person,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76,
        columnNumber: 29
      }
    }, __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.commentBlock,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77,
        columnNumber: 33
      }
    }, login), __jsx("span", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.commentMoment,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80,
        columnNumber: 33
      }
    }, "comented this issue", moment__WEBPACK_IMPORTED_MODULE_4___default()(created_at).fromNow())) : __jsx(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3___default.a, {
      height: 29,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 30
      }
    }), !loading ? __jsx("div", {
      className: _styles_scss__WEBPACK_IMPORTED_MODULE_7___default.a.body,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 29
      }
    }, body) : __jsx(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3___default.a, {
      height: 350,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92,
        columnNumber: 30
      }
    }))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(state => state)(Issue));

/***/ }),

/***/ "./pages/styles.scss":
/*!***************************!*\
  !*** ./pages/styles.scss ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
	"titleIssue": "_13IBBD-c10lpDX5Cx8Ddqb",
	"statusIssue": "_1_P0jWnr9P8KbAlGpLoSD8",
	"outlineIcon": "_3dw4_2_kH-scykKXcXluFZ",
	"login": "-jnXChe1iZx07l7U3NRjo",
	"moment": "_1OLda4PJWC8GvqKjV-lf47",
	"imageWrapper": "_1RbjjqyDH02az8hoVjoCsX",
	"arrowBack": "_2GZz4xBd_IXrhGxYIdBmZv",
	"commentBlock": "_1pBYyLbGevSs2tlaC7uAXq",
	"commentMoment": "_3GYY6S2Z1-PmqoIgWM_hxF",
	"aboutCompany": "JfIMAW3O728D4W8InCBwa",
	"aboutImg": "_2lg5DbPeWUMXxzgOpA9HFF",
	"textAbout": "_13DEwQ8FxGQtzgcrYQ5RD",
	"breadcrumbs": "JIzGuMrWaJnwJvtyxj75i",
	"breadcrumbsLinks": "gKYDiVPy1OlQoHgxgjHim",
	"active": "_26Tc2kOxRMQ-eylVkbvMYl",
	"block": "BWB4CFqryEzsafnCu19EH",
	"header": "_2jJ0u2IP9EFPvnZBY8TOVf",
	"gitLogo": "_2fLcD8ryI1jWx26Utl1HgB",
	"wrapper": "_2a_plItHO16c59wIwMCSvx",
	"content": "_3xqyrH14vpQPdPdkv21GGO",
	"controls": "NhWg3wrkjBntoI1ymaaHM",
	"issueWrapper": "_3OTRj7cXo7rqGkViAe7-QD",
	"image": "_1UUknJgo4eYv1kXqtQjddS",
	"person": "_3Qq-uBj6FvSn7JNUn0Cb5O",
	"error": "_2q8zl7qJI59LE-rO5CWLq5",
	"body": "_1s_UOdFxuK0GKll0UEZuUt",
	"pagination": "_2gtQh16urWoWz3k5HHktNM",
	"searchButton": "_2Z3QeIiN1fRdNjIZJxXPQl"
};

/***/ }),

/***/ 5:
/*!************************************!*\
  !*** multi ./pages/issues/[id].js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\feel_\OneDrive\Desktop\projects\githubissues\pages\issues\[id].js */"./pages/issues/[id].js");


/***/ }),

/***/ "@material-ui/icons/ArrowBackIos":
/*!**************************************************!*\
  !*** external "@material-ui/icons/ArrowBackIos" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ArrowBackIos");

/***/ }),

/***/ "@material-ui/icons/ErrorOutline":
/*!**************************************************!*\
  !*** external "@material-ui/icons/ErrorOutline" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ErrorOutline");

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-loading-skeleton":
/*!*****************************************!*\
  !*** external "react-loading-skeleton" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-loading-skeleton");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ })

/******/ });
//# sourceMappingURL=[id].js.map