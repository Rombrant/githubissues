const compression = require('compression');
const useragent = require('express-useragent');
const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const port = process.env.PORT || 3000;


app.prepare().then(() => {
  const server = express();

  server.use(compression());

  server.use(useragent.express());

  server.use('/service-worker.js', express.static('/service-worker.js'));

  server.use(bodyParser.json());

  server.get('*', (req, res) => {
    return handle(req, res)
  });

  /**
   * SEO
   * */
  server.get('/robots.txt', (req, res) => {
    res.sendFile(`${process.cwd()}/robots.txt`);
  });


  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Read on http://localhost:${port}`)
  })
});
