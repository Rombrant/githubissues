export const actionTypes = {
    ENTER_NAME: 'ENTER_NAME',
    ENTER_REPOS: 'ENTER_REPOS',
    CHOOSE_ITEM_PERPAGE: 'CHOOSE_ITEM_PERPAGE',
    COMMON_ISSUES_INFO_REQUEST: 'COMMON_ISSUES_INFO_REQUEST',
    COMMON_ISSUES_INFO_SUCCESS: 'COMMON_ISSUES_INFO_SUCCESS',
    COMMON_ISSUES_INFO_FAIL: 'COMMON_ISSUES_INFO_FAIL',
    LOAD_DATA_REQUEST: 'LOAD_DATA_REQUEST',
    LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
    LOAD_DATA_FAIL: 'LOAD_DATA_FAIL',
    CHOOSE_PAGE: 'CHOOSE_PAGE',
    CHOOSE_PATH: 'CHOOSE_PATH',
    ISSUE_DETAIL_REQUEST: 'ISSUE_DETAIL_REQUEST',
    ISSUE_DETAIL_SUCCESS: 'ISSUE_DETAIL_SUCCESS',
    ISSUE_DETAIL_FAIL: 'ISSUE_DETAIL_FAIL',
  }

  export function enterName(name) {
    return {
      type: actionTypes.ENTER_NAME,
      name
    }
  }

  export function enterRepos(repos) {
    return {
      type: actionTypes.ENTER_REPOS,
      repos
    }
  }

  export function chooseItemPerPage(count) {
    return {
      type: actionTypes.CHOOSE_ITEM_PERPAGE,
      count
    }
  }

  export function fetchCommonIssuesRequest() {
    return {
      type: actionTypes.COMMON_ISSUES_INFO_REQUEST
    }
  }

  export function fetchCommonIssuesFail() {
    return {
      type: actionTypes.COMMON_ISSUES_INFO_FAIL
    }
  }

  export function fetchCommonIssuesSuccess(pagination, total) {
    return {
      type: actionTypes.COMMON_ISSUES_INFO_SUCCESS,
      payload: {
        pagination: pagination,
        total: total
      }
    }
  }

  export function loadDataRequest() {
    return {
      type: actionTypes.LOAD_DATA_REQUEST
    }
  }

  export function loadDataSuccess(list) {
    return {
      type: actionTypes.LOAD_DATA_SUCCESS,
      list
    }
  }

  export function loadDataFail() {
    return {
      type: actionTypes.LOAD_DATA_FAIL
    }
  }

  export function choosePage(page) {
    return {
      type: actionTypes.CHOOSE_PAGE,
      page
    }
  }

  export function choosePath(num) {
    return {
      type: actionTypes.CHOOSE_PATH,
      num
    }
  }

  export function issueDetailRequest(repos, name, num) {
    return {
      type: actionTypes.ISSUE_DETAIL_REQUEST,
      payload: {
        repos: repos, 
        name: name,
        num: num
      }
    }
  }

  export function issueDetailSuccess(issue) {
    return {
      type: actionTypes.ISSUE_DETAIL_SUCCESS,
      issue
    }
  }

  export function issueDetailFail() {
    return {
      type: actionTypes.ISSUE_DETAIL_FAIL
    }
  }