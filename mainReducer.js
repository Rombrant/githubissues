import { actionTypes } from './actions'

export const exampleInitialState = {
  repos: '',
  name: '',
  list: [],
  perPage: '10',
  total: '',
  pagination: [],
  curPage: '1',
  isFetched: false,
  loading: false,
  path: '',
  error: false
}

function mainReducer(state = exampleInitialState, action) {
  switch (action.type) {

    case actionTypes.ENTER_NAME:
      return {
        ...state,
        name: action.name,
        error: false
      }

    case actionTypes.ENTER_REPOS:
      return {
        ...state,
        repos: action.repos,
        error: false
      }

    case actionTypes.CHOOSE_PAGE:
      return {
        ...state,
        curPage: action.page,
        error: false
      }

    case actionTypes.CHOOSE_PATH:
      const { repos, name } = state;
      const { num } = action;
      const path = `https://api.github.com/repos/${repos}/${name}/issues/${num}`;
      return {
        ...state,
        path: path,
        error: false
      }

    case actionTypes.CHOOSE_ITEM_PERPAGE:
      const { total } = state;
      let pagination = [];
      if (total) {
        const { count } = action;
        const totalpage = Math.round(Number(total) / Number(count));
        for ( let i = 0; totalpage > i; i++) {
            pagination.push(i+1);
        }
      }
      return {
        ...state,
        perPage: action.count,
        pagination: pagination,
        curPage: '1'
      }

    case actionTypes.COMMON_ISSUES_INFO_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      }

    case actionTypes.COMMON_ISSUES_INFO_SUCCESS:
      return {
        ...state,
        pagination: action.payload.pagination,
        total: action.payload.total,
        error: false
      }

    case actionTypes.COMMON_ISSUES_INFO_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Something went wrong please reboot your page'
      }
      
    case actionTypes.LOAD_DATA_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      }

    case actionTypes.LOAD_DATA_SUCCESS:
      return {
        ...state,
        list: action.list,
        isFetched: true,
        loading: false,
        error: false
      }
      
    case actionTypes.LOAD_DATA_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Something went wrong please reboot your page'
      }

    default:
      return state
  }
}

export default mainReducer