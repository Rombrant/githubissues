import React from 'react';
import s from './styles.scss';

export default function Searching(props) {
    return(
        <div>
            <input className={s.input} onKeyPress={props.fetchResult} onChange={props.inputChange}></input>
        </div>
    )
}