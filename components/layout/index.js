import React from 'react';
import s from '../../pages/styles.scss';

export default function Layout(props) {
    return(
        <div className={s.wrapper}>
            <header className={s.header}>
                <div className={s.gitLogo} />
                <h1>Github Issue Tracker</h1>
            </header>
            {props.children}
        </div>
    )
};