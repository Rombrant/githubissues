import React from 'react';
import Link from 'next/link'
import moment from 'moment';
import s from './style.scss';

export default function List(props) {
    return(
        <ul>
            {props.list.map((e, index) => (
                <li className={s.block} key={index}>
                    <div style={{ display: 'flex', paddingLeft: '20px', alignItems: 'center' }}>
                        <div className={s.image} style={{ 
                            background: `url('${e.avatar}')`,
                            backgroundSize: 'cover',
                            borderRadius: '50%'
                            }} />
                            <Link href={{ pathname: `issues/${e.number}`, query: { repos: props.repos, name: props.name, num: e.number} }} >
                                <div className={s.text} >{e.title}</div>
                            </Link>
                        </div>
                    <div className={s.time}>#{e.number} opened at {moment(e.date).format('LL')}</div>
                </li>
            ))}
        </ul>
    )
}