const withSass = require('@zeit/next-sass')
const withImages = require('next-images')
const withFonts = require('next-fonts');
const webpack = require('webpack');

module.exports = withFonts(withImages(withSass({
    cssModules: true,
    webpack(config, options) {
        const extendedConfig = { ...config }

        if (options.isServer) {

            extendedConfig.module.rules = [
                ...config.module.rules,
                {
                    test: /\.jsx?$/,
                    use: [
                        options.defaultLoaders.babel
                    ]
                }
            ]
        }

        extendedConfig.plugins = [
            ...config.plugins,
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
                __SERVER__: JSON.stringify(options.isServer),
                __CLIENT__: JSON.stringify(!options.isServer)
            })
        ]

        return extendedConfig
    }

})))
