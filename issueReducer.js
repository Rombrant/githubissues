import { actionTypes } from './actions'

export const exampleInitialState = {
    state: '',
    body: '',
    author_association: '',
    created_at: '',
    title: '',
    number: '',
    login: '',
    avatar_url: '',
    loading: false,
    error: false,
    name: '',
    repos: '',
    num: ''
}

function issueReducer(state = exampleInitialState, action) {
  switch (action.type) {

    case actionTypes.ISSUE_DETAIL_REQUEST:
      return {
        ...state,
        name: action.payload.name,
        repos: action.payload.repos,
        num: action.payload.num,
        loading: true,
        error: false
      }

    case actionTypes.ISSUE_DETAIL_SUCCESS:
      return {
        ...action.issue,
        loading: false,
        error: false
      }

    case actionTypes.ISSUE_DETAIL_FAIL:
      return {
        ...state,
        loading: false,
        error: true
      }

    default:
      return state
  }
}

export default issueReducer